# SOLID Design Principles 
SOLID principles are a set of rules or guidelines which should be followed while developing software. There are following five subclasses of SOLID principles:
1. **Single Responsibility Principle**
1. **Open/Closed Principle**
1. **Liskov Substitution Principle**
1. **Interface Segregation Principle**
1. **Dependency Inversion Principle**
---
## Single Responsibility Principle 
>*Robert C. Martin* expresses the principle as, "A class should have only one reason to change". 
Here class represents a single unit of work.
```java
public class Do{
        
    public Connnection getConnection()
        
    public Data getReadData()

    public void releaseResources()
}
```
In above class *Do* is doing 3 task which is not good practice insted all three should be separateed. Below given code structure will be the better approch:
```java
public class Resources {
    public Connection getConnection(){
        ....
        return connection;
    }
}

public class Read {
    public Data getData(){
        ....
        return data;
    }
}
public class Release{
    public void releaseResouces(){
        ....
    }
}
```

## Open/Closed Principle
>"Software entities should be open for extension, but closed for modification"

Let's take an example:
```java
class Rectangle {
    int length;
    int breadth;

    public Rectangle(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    public int getLength() {
        return length;
    }

    public int getBreadth() {
        return breadth;
    }
}

class Circle {
    double radius;
    
    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
}

public double sumArea(Object[] shapes){
    double sum = 0;
    double area;
    for(Object o : shapes){
        if (Reatangle.class == o.getClass()) {
            Rectangle r = (Rectangle)o; 
            area = r.getLength()*r.getBreadth();
        }else {
            Circle c = (Circle)o; 
            area = 3.14*c.getRadius()*c.getRadius();
        }
        sum += area;
    }
    return area;
}
```
In ablove code snippet *sumArea()* calculates the area of list of different shapes. This solution will work but if we want to add another shape like triangle we have to make changes in the *sumArea()* method. 

Now look at the below code snippet:

```java
    abstract class Shape {
        public double area();
    }

    class Rectangle implements Shape {
        double lenght;
        double breadth;
        public Rectangle(double length, double breadth){
            this.length = length;
            this.breadth = breadth;
        }
        @Override*
        public double area(){
            return length*breadth;
        }
    }

    class Circle implements Shape {
        double radius;
        public Circle(double radius){
            this.radius = radius;
        }
        @Override
        public double area(){
            return 3.14*radius*radius;
        }
    }

    public double sumArea(Shape[] shapes){
        double sum = 0;
        for(Shape shape : shapes){
            sum += shape.area();
        }
        return sum;
    }
```
This code is open for the extension, you can pass any type of shape provided it implements *Shape* interface then you have to provide implementation of the *area()* method mean while you don't have to modify the _sumArea()_ method this why the *sumArea()* method is closed for modification.

## Liskov Substitution Principle
> Introduced by Barbara Liskov state that "object in a program should be replaceable with instances of their subtypes without altering the correctness of that program"

According to it whenever a superclass is inherited to subclass. The implementation should be in a way that a reference variable of type superclass can refer to the object of subclass like:

```java
    Set<Integer> set = new HashSet<Integer>();
```

Here HashSet implements Set and referring to it using reference type of set is fine.

## Interface Segregation Principle
> "Many client-specific interfaces are better than one general-purpose interface"

According to this, an interface should contain the least no. of methods possible. Because if there is any changes in the interface all the implementing classes have to change so instead of providing a big interface provide many small interfaces. Let's take an example:
```java
public interface Car {

    public void drive();

    public void turnOnLights();

    public void pushRearBreaks();

}
```
In above code code block Car is an interface with three defferent methods. Suppose if any other class implements is it have to override all the methods and in case if the class does not have any lights in it it have to provide a dumy implementation to *turnOnLights()* in order to keep compiler happy. Let's take another example: 
```java
public interface Drivable {
    public void drive();
}

public interface Startable {
    public void start();
}

public interface Stopable {
    public void Stop();
}

public class Bike implements Drivable, Starable  {
    @Override
    public void drive(){
        ...
    }

    @Override
    public void start(){
        ...
    }
}
```
In above example we splitted the interfaces into smaller ones therefore programmer don't have to give dummy body to unwanted methods. 
## Dependency Inversion Principle
>One should "depend upon abstraction, [not] concretions"

According to it, modules of the code should be more abstracted and dependent on higher abstract. This makes code flexible. Let's take an example:

```java
class Scanner {
    public boolean hasNext(){
        ...
    }
    public char next(){
        ...
    }
}

class Display {
    public print(){
        ...//Print on Display
    }
}
public void copy(Scanner scanner, Display display){
    while(scanner.hasNext()){
        display.print(scanner.next());
    }
}
```
In above example the Scanner and Display is user defined classes with some methods. And *copy()* method copies data from Scanner to Display. Suppose in case we want to copy data from Scanner to Printer we have make chnages in *copy()* method. Let's take another example:

```java
public interface Reader{
    public Object read();
}
public interface Writer{
    public write(Object o);
}
class Scanner implements Reader{
    
    ...
    
    @Override
    public Object read(){
        ...
    }
}

class Display implements Writer{
    
    ...
    
    @Override
    public void write(Object){
        ...
    }
}
public void copy(Reader r, Writer o){
    w.write(r,read());
}
```
Consider the above code here *copy()* method can copy from any class who implements *Reader* to any class who implements *Writer*. 

Following these SOLID principles of design code will be:
* Loosely coupled 
* Easily upgradable 
* More readable
___
### Resources:
[Introduction to SOLID Principles](https://www.youtube.com/watch?v=yxf2spbpTSw "YouTube Video 1")

[SOLID Design Principles Introduction](https://youtu.be/HLFbeC78YlU?list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme "YouTube Video 2")

[Markdown Crash Course](https://www.youtube.com/watch?v=HUBNt18RFbo "YouTube Video 3")
